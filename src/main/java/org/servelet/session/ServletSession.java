package org.servelet.session;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class ServletSession
 */
@WebServlet("/servletsession")
public class ServletSession extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ServletSession() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request,response);
	}
	protected void processRequest(HttpServletRequest req,
	          HttpServletResponse resp) throws ServletException, IOException {
	        req.setCharacterEncoding("UTF-8");
	        resp.setContentType("text/html;charset=UTF-8");
	        PrintWriter out = resp.getWriter();

	        out.println("<html>");
	        out.println("<head>");
	        out.println("<title>Session</title>");
	        out.println("</head>");
	        out.println("<body>");
	        String page = req.getParameter("page");

	        out.println("<form action=\"servletsession\" method=\"post\">");
	        if("page1".equals(page)) {
	            out.println("問題一：<input type=\"text\" name=\"p1q1\"><br>");
	            out.println("問題二：<input type=\"text\" name=\"p1q2\"><br>");
	            out.println(
	                    "<input type=\"submit\" name=\"page\" value=\"page2\">");
	        }
	        else if("page2".equals(page)) {
	            String p1q1 = req.getParameter("p1q1");
	            String p1q2 = req.getParameter("p1q2");
	            HttpSession session = req.getSession();
	            session.setAttribute("p1q1", p1q1);
	            session.setAttribute("p1q2", p1q2);
	            out.println("問題三：<input type=\"text\" name=\"p2q1\"><br>");
	            out.println(
	                    "<input type=\"submit\" name=\"page\" value=\"finish\">");
	        }
	        else if("finish".equals(page)) {
	            HttpSession session = req.getSession();
	            out.println(session.getAttribute("p1q1") + "<br>");
	            out.println(session.getAttribute("p1q2") + "<br>");
	            out.println(req.getParameter("p2q1") + "<br>");
	        }
	        out.println("</form>");

	        out.println("</body>");
	        out.println("</html>");
	        out.close();
	    } 
}
