package org.servelet.session;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class UrlRewrite
 */
@WebServlet("/urlrewrite")
public class UrlRewrite extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UrlRewrite() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        out.println("<html>");
        out.println("<head>");
        out.println("<title>Servlet Search</title>");
        out.println("</head>");
        out.println("<body>");

        String start = request.getParameter("start");
        if (start == null) {
            start = "1";
        }

        int count = Integer.parseInt(start);
        int begin = 10 * count - 9;
        int end = 10 * count;
        out.println("第 " + begin + " 到 " + end + " 搜尋結果<br>");
        out.println("<ul>");
        for(int i = 1; i <= 10; i++) {
            out.println("<li>搜尋結果" + i + "</li>");
        }
        out.println("</ul>");
        for (int i = 1; i < 10; i++) {
            if (i == count) {
                out.println(i);
                continue;
            }
            out.println("<a href=\"urlrewrite?start=" + i + "\">" + i + "</a>");
        }
        out.println("</body>");
        out.println("</html>");
        out.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
