package org.servlet.filter;

import java.io.IOException;
import java.util.zip.GZIPOutputStream;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet Filter implementation class CompressionFilter
 */
//@WebFilter(filterName = "CompressionFilter", urlPatterns = { "/*" },asyncSupported=true)
public class CompressionFilter implements Filter {
	public void init(FilterConfig filterConfig) {
	}

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;

		String encodings = req.getHeader("accept-encoding");
		if ((encodings != null) && (encodings.indexOf("gzip") > -1)) {
			CompressionResponseWrapper responseWrapper = new CompressionResponseWrapper(
					res);
			responseWrapper.setHeader("content-encoding", "gzip");

			chain.doFilter(request, responseWrapper);

			GZIPOutputStream gzipOutputStream = responseWrapper
					.getGZIPOutputStream();
			if (gzipOutputStream != null) {
				gzipOutputStream.finish();
			}
		} else {
			chain.doFilter(request, response);
		}
	}

	public void destroy() {
	}
}
