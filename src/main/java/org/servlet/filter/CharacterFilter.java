package org.servlet.filter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.HttpServletRequest;

/**
 * Servlet Filter implementation class CharacterFilter
 */
//@WebFilter(filterName = "CharacterFilter", urlPatterns = { "/guestbook.do" },asyncSupported=true,initParams = { @WebInitParam(name = "ESCAPE_LIST", value = "/WEB-INF/escapelist.txt") })
public class CharacterFilter implements Filter {
	private Map<String, String> escapeMap;

	/**
	 * Default constructor.
	 */
	public CharacterFilter() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		// TODO Auto-generated method stub
		HttpServletRequest requestWrapper =new CharacterRequestWrapper(escapeMap,(HttpServletRequest) request);
         chain.doFilter(requestWrapper, response);
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig filterConfig) throws ServletException {
		BufferedReader reader = null;
		try {
			String escapeListFile = filterConfig.getInitParameter("ESCAPE_LIST");
			reader = new BufferedReader(new InputStreamReader(filterConfig.getServletContext().getResourceAsStream(escapeListFile)));
			String input = null;
			escapeMap = new HashMap<String, String>();
			while ((input = reader.readLine()) != null) {
				String[] tokens = input.split("\\t");
				escapeMap.put(tokens[0], tokens[1]);
			}
		} catch (IOException ex) {
			Logger.getLogger(CharacterFilter.class.getName()).log(Level.SEVERE,
					null, ex);
		} finally {
			try {
				reader.close();
			} catch (IOException ex) {
				Logger.getLogger(CharacterFilter.class.getName()).log(
						Level.SEVERE, null, ex);
			}
		}
	}

}
