package org.servlet.filter;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

public class CharacterRequestWrapper extends HttpServletRequestWrapper{
	private Map<String, String> escapeMap;
	public CharacterRequestWrapper(Map<String, String> map,HttpServletRequest request) {
		super(request);
		this.escapeMap=map;
	}
	@Override
    public String getParameter(String name) {
        return doEscape(getRequest().getParameter(name));
    }

    private String doEscape(String parameter) {
        if(parameter == null) {
            return null;
        }
        String result = parameter;
        for(String origin : escapeMap.keySet()) {
            String escape = escapeMap.get(origin);
            result = result.replaceAll(origin, escape);
        }
        return result;
    }
}
