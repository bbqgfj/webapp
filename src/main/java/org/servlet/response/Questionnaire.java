package org.servlet.response;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Questionnaire
 */
@WebServlet("/questionnaire")
public class Questionnaire extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Questionnaire() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request,response);
		}
	 protected void processRequest(HttpServletRequest req, 
	            HttpServletResponse resp) throws ServletException, IOException {
	        req.setCharacterEncoding("UTF-8");
	        resp.setContentType("text/html;charset=UTF-8");
	        
	        PrintWriter out = resp.getWriter();
	        out.println("<html>");
	        out.println("<head>");
	        out.println("<title>Questionnaire</title>");
	        out.println("</head>");
	        out.println("<body>");

	        String page = req.getParameter("page");
	        out.println("<form action=\"questionnaire\" method=\"post\">");

	        if("page1".equals(page)) {          // 第一頁問卷
	            out.println("問題一：<input type=\"text\" name=\"p1q1\"><br>");
	            out.println("問題二：<input type=\"text\" name=\"p1q2\"><br>");
	            out.println(
	                    "<input type=\"submit\" name=\"page\" value=\"page2\">");
	        }
	        else if("page2".equals(page)) {    // 第二頁問卷
	            String p1q1 = req.getParameter("p1q1");
	            String p1q2 = req.getParameter("p1q2");
	            out.println("問題三：<input type=\"text\" name=\"p2q1\"><br>");
	            out.println("<input type=\"hidden\" name=\"p1q1\" value=\"" +
	                           p1q1 + "\">");
	            out.println("<input type=\"hidden\" name=\"p1q2\" value=\"" +
	                           p1q2 + "\">");
	            out.println(
	                    "<input type=\"submit\" name=\"page\" value=\"finish\">");
	        }
	        else if("finish".equals(page)) {    // 最後答案收集
	            out.println(req.getParameter("p1q1") + "<br>");
	            out.println(req.getParameter("p1q2") + "<br>");
	            out.println(req.getParameter("p2q1") + "<br>");
	        }
	        out.println("</form>");
	        out.println("</body>");
	        out.println("</html>");
	        out.close();
	    } 
}
