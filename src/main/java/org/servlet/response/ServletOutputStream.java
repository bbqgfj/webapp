package org.servlet.response;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ServletOutputStream
 */
@WebServlet("/servletoutputstream")
public class ServletOutputStream extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ServletOutputStream() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setContentType("image/jpeg");

		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < 4; i++) {
			int number = (int) (Math.random() * 10);
			builder.append(number);
		}

		BufferedImage password = getPasswdImage(builder.toString());

		// 將產生的驗證碼存在某個地方，以供之後比對

		OutputStream out = response.getOutputStream();
		ImageIO.write(password, "JPG", out);
		out.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	public BufferedImage getPasswdImage(String password) throws IOException {
		BufferedImage bufferedImage = new BufferedImage(60, 20,
				BufferedImage.TYPE_INT_RGB);
		Graphics g = bufferedImage.getGraphics();
		g.setColor(Color.WHITE);
		g.setFont(new Font("標楷體", Font.BOLD, 16));
		g.drawString(password, 10, 15);
		return bufferedImage;
	}
}
