package org.servlet.async;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.AsyncContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * Application Lifecycle Listener implementation class WebInitListener
 * 
 */
@WebListener
public class WebInitListener implements ServletContextListener {

	/**
	 * Default constructor.
	 */
	public WebInitListener() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see ServletContextListener#contextInitialized(ServletContextEvent)
	 */
	private List<AsyncContext> asyncs = new ArrayList<AsyncContext>();

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		sce.getServletContext().setAttribute("asyncs", asyncs);
		new Thread(new Runnable() {
			@Override
			public void run() {
				while (true) {
					try {
						// 模擬不定時
						Thread.sleep((int) (Math.random() * 10000));
						// 隨機產生數字
						double num = Math.random() * 10;
						synchronized (asyncs) {
							// 逐一完成非同步請求
							for (AsyncContext ctx : asyncs) {
								ctx.getResponse().getWriter().println(num);
								ctx.complete();
							}
							asyncs.clear();
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}).start();
	}

	/**
	 * @see ServletContextListener#contextDestroyed(ServletContextEvent)
	 */
	public void contextDestroyed(ServletContextEvent sce) {
		// TODO Auto-generated method stub
	}

}
