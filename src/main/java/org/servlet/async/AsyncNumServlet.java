package org.servlet.async;

import java.io.IOException;
import java.util.List;

import javax.servlet.AsyncContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class AsyncNumServlet
 */
@WebServlet(name = "AsyncNumServlet", urlPatterns = { "/asyncNum.do" }, asyncSupported = true)
public class AsyncNumServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private List<AsyncContext> asyncs;

	@SuppressWarnings("unchecked")
	@Override
	public void init() throws ServletException {
		asyncs = (List<AsyncContext>) getServletContext().getAttribute("asyncs");
	}

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		AsyncContext ctx = request.startAsync();
		synchronized (asyncs) {
			asyncs.add(ctx);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
	}

}
