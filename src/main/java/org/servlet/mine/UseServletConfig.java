package org.servlet.mine;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class UseServletConfig
 */
@WebServlet(urlPatterns = { "/useservletconfig" }, initParams = {
		@WebInitParam(name = "SUCCESS", value = "user.view"),
		@WebInitParam(name = "ERROR", value = "loginFail.html") })
public class UseServletConfig extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private String success;
	private String failed;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UseServletConfig() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public void init() throws ServletException {
		super.init();
		success = this.getInitParameter("SUCCESS");
		failed = this.getInitParameter("ERROR");
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");
		String name = request.getParameter("name");
		String passwd = request.getParameter("passwd");
		if ("qianjin".equals(name) && "123456".equals(passwd)) {
			request.setAttribute("user", "qianjin");
			request.getRequestDispatcher(success).forward(request,
					response);
		} else {
			request.getRequestDispatcher(failed).forward(request, response);
		}
	}

}
