package org.servlet.mine;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class UrlPattern
 */
//http://localhost:8080/webapp/servlet/path.view
@WebServlet(name="Path", urlPatterns={"/servlet/*"})
public class UrlPattern extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UrlPattern() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out=response.getWriter();
		 out.println("<html>");
	     out.println("    <head>");
	     out.println("        <title>Servlet ShowHeader</title>");
	     out.println("    </head>");
	     out.println("    <body>");
	     out.println(request.getRequestURI() + "<br>");
	     out.println(request.getContextPath() + "<br>");
	     out.println(request.getServletPath() + "<br>");
	     out.println(request.getPathInfo());
	     out.println("    </body>");
	     out.println("</html>");
	     out.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request,response);
	}

}
