package org.servlet.mine;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class UseServletContext
 */
@WebServlet(urlPatterns={"/useservletcontext"},initParams={
        @WebInitParam(name="PDF_FILE", value="/file/dddquickly-chinese-2014version.pdf"),
        @WebInitParam(name="FILE_LIST", value="/"),
    })
public class UseServletContext extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private String PDF_FILE;
	private String FILE_LIST;
	@Override
	public void init() throws ServletException {
		super.init();
		PDF_FILE = this.getInitParameter("PDF_FILE");
		FILE_LIST = this.getInitParameter("FILE_LIST");
	}
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UseServletContext() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String file=request.getParameter("file");
		if(file.equals("pdf")){
			postPDF(request,response);
		}else{
			postFileList(request,response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}
	private void postPDF(HttpServletRequest request, HttpServletResponse response) throws IOException{
		response.setContentType("application/pdf");
        InputStream in = this.getServletContext().getResourceAsStream(PDF_FILE);
        OutputStream out = response.getOutputStream();
        byte[] buffer = new byte[1024];
        int length = -1;
        while((length = in.read(buffer)) != -1) {
            out.write(buffer, 0, length);
        }
        in.close();
        out.close();
	}
	private void postFileList(HttpServletRequest request, HttpServletResponse response) throws IOException{
		response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<head>");
        out.println("<title>use servlet context</title>");
        out.println("</head>");
        out.println("<body>");
        for (String fileName : getServletContext()
                                .getResourcePaths(FILE_LIST)) {
            fileName = fileName.replaceFirst("/", "");
            out.println("<p>" + fileName + "</p>");
        }
        out.println("</body>");
        out.println("</html>");
        out.close();
	}
}
