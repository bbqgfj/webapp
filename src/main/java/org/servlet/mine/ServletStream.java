package org.servlet.mine;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ServletStream
 */
@WebServlet("/servletstream")
public class ServletStream extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ServletStream() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String contentType = request.getContentType();
        int formDataLength = request.getContentLength();

        DataInputStream dataStream =
                new DataInputStream(request.getInputStream());
        byte datas[] = new byte[formDataLength];
        int totalBytes = 0;
        while (totalBytes < formDataLength) {
            int bytes = dataStream.read(datas, totalBytes, formDataLength);
            totalBytes += bytes;
        }

        String reqBody = new String(datas, "ISO-8859-1");
        String filename = reqBody.substring(reqBody.indexOf("filename=\"") + 10);
        filename = filename.substring(0, filename.indexOf("\""));
        filename = filename.substring(filename.lastIndexOf("\\\\") + 1, filename.indexOf("\""));
        String boundary = contentType.substring(contentType.lastIndexOf("=") + 1, contentType.length());
        int pos;
        pos = reqBody.indexOf("filename=\"");
        pos = reqBody.indexOf("\n", pos) + 1;
        pos = reqBody.indexOf("\n", pos) + 1;
        pos = reqBody.indexOf("\n", pos) + 1;
        int boundaryLoc = reqBody.indexOf(boundary, pos) - 4;
        int startPos = ((reqBody.substring(0, pos)).getBytes("ISO-8859-1")).length;
        int endPos = ((reqBody.substring(0, boundaryLoc)).getBytes("ISO-8859-1")).length;
        File file=new File("C:/Users/qiannjin/workspace/" + filename);
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(datas, startPos, (endPos - startPos));
        fileOutputStream.flush();
        fileOutputStream.close();
    }
}
