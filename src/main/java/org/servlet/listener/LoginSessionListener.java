package org.servlet.listener;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * Application Lifecycle Listener implementation class LoginSessionListener
 *
 */
@WebListener
public class LoginSessionListener implements HttpSessionListener {
	private static int count;

    public static int getCount() {
        return count;
    }
    /**
     * Default constructor. 
     */
    public LoginSessionListener() {        
    }

	/**
     * @see HttpSessionListener#sessionCreated(HttpSessionEvent)
     */
    public void sessionCreated(HttpSessionEvent se) {
        count++;
    }

	/**
     * @see HttpSessionListener#sessionDestroyed(HttpSessionEvent)
     */
    public void sessionDestroyed(HttpSessionEvent se) {
        count--;
    }
	
}
