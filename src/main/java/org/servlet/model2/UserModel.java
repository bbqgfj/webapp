package org.servlet.model2;

import java.util.HashMap;
import java.util.Map;

public class UserModel {
private Map<String, String> messages;
    
    public UserModel() {
        messages = new HashMap<String, String>();
        messages.put("caterpillar", "Hello");
        messages.put("Justin", "Welcome");
        messages.put("momor", "Hi");
    }

    public String getMessage(String user) {
        String message = messages.get(user);
        return message + ", " + user + "!";
    }
}
